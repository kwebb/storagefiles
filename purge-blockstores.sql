
DELETE from blockstores;
DELETE from blockstore_attributes;
DELETE from blockstore_state;
DELETE from blockstore_trees;
DELETE from blockstore_type_attributes;

DELETE from nodes where node_id in ('dbox1','dbox2','dbox3');
DELETE from node_features where node_id in ('dbox1','dbox2','dbox3');
DELETE from interfaces where node_id in ('dbox1','dbox2','dbox3');
DELETE from interface_state where node_id in ('dbox1','dbox2','dbox3');
DELETE from wires where node_id1 in ('dbox1','dbox2','dbox3');
DELETE from node_auxtypes where node_id in ('dbox1','dbox2','dbox3');
DELETE from reserved where node_id in ('dbox1','dbox2','dbox3');
