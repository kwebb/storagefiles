REPLACE INTO `blockstores` VALUES ('101','dbox1','sas-1','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('102','dbox1','sas-2','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('103','dbox1','sas-3','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('104','dbox1','sas-4','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('105','dbox1','sas-5','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('106','dbox1','sas-6','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('107','dbox1','sas-7','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('108','dbox1','sas-8','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('109','dbox1','sas-9','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('110','dbox1','sas-10','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('111','dbox1','sas-11','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('112','dbox1','sas-12','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('120','dbox1','rz-1','zfs-raidz','compound','2724605','0',NOW());
REPLACE INTO `blockstores` VALUES ('121','dbox1','rz-2','zfs-raidz','compound','2724605','0',NOW());

REPLACE INTO `blockstores` VALUES ('201','dbox2','sas-1','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('202','dbox2','sas-2','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('203','dbox2','sas-3','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('204','dbox2','sas-4','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('205','dbox2','sas-5','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('206','dbox2','sas-6','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('207','dbox2','sas-7','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('208','dbox2','sas-8','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('209','dbox2','sas-9','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('210','dbox2','sas-10','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('211','dbox2','sas-11','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('212','dbox2','sas-12','c2100-sas','element','571808','0',NOW());
REPLACE INTO `blockstores` VALUES ('220','dbox2','rz-1','zfs-raidz','compound','2724605','1',NOW());
REPLACE INTO `blockstores` VALUES ('221','dbox2','rz-2','zfs-raidz','compound','2724605','1',NOW());

REPLACE INTO `blockstore_attributes` VALUES ('101','serialnum','JZY5517J','string');
REPLACE INTO `blockstore_attributes` VALUES ('102','serialnum','JZY54UEJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('103','serialnum','JZY3XSUJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('104','serialnum','JZY54UWJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('105','serialnum','JZY561DJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('106','serialnum','JZY3XUTJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('107','serialnum','JZY54VJJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('108','serialnum','JZY54Z1J','string');
REPLACE INTO `blockstore_attributes` VALUES ('109','serialnum','JZY54VUJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('110','serialnum','JZY5506J','string');
REPLACE INTO `blockstore_attributes` VALUES ('111','serialnum','JZXKSM5J','string');
REPLACE INTO `blockstore_attributes` VALUES ('112','serialnum','JZY3XN7J','string');

REPLACE INTO `blockstore_attributes` VALUES ('201','serialnum','JZY3WNHJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('202','serialnum','JZY55X3J','string');
REPLACE INTO `blockstore_attributes` VALUES ('203','serialnum','JZY4PAPJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('204','serialnum','JZY4GX4J','string');
REPLACE INTO `blockstore_attributes` VALUES ('205','serialnum','JZY4NYUJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('206','serialnum','JZY4PBTJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('207','serialnum','JZY4NPYJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('208','serialnum','JZY4NV8J','string');
REPLACE INTO `blockstore_attributes` VALUES ('209','serialnum','JZY54ZRJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('210','serialnum','JZY54SBJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('211','serialnum','JZY3XVUJ','string');
REPLACE INTO `blockstore_attributes` VALUES ('212','serialnum','JZY54GTJ','string');

REPLACE INTO `blockstore_type_attributes` VALUES ('c2100-sas','class','local','string','1');
REPLACE INTO `blockstore_type_attributes` VALUES ('c2100-sas','protocol','SAS','string','1');
REPLACE INTO `blockstore_type_attributes` VALUES ('c2100-sas','rot-speed','15000','integer','0');
REPLACE INTO `blockstore_type_attributes` VALUES ('c2100-sas','enclosure','1','integer','0');
REPLACE INTO `blockstore_type_attributes` VALUES ('zfs-mirror','class','SAN','string','1');
REPLACE INTO `blockstore_type_attributes` VALUES ('zfs-mirror','protocol','iSCSI','string','1');
REPLACE INTO `blockstore_type_attributes` VALUES ('zfs-mirror','divisible','1','boolean','0');
REPLACE INTO `blockstore_type_attributes` VALUES ('zfs-raidz','class','SAN','string','1');
REPLACE INTO `blockstore_type_attributes` VALUES ('zfs-raidz','protocol','iSCSI','string','1');
REPLACE INTO `blockstore_type_attributes` VALUES ('zfs-raidz','divisible','1','boolean','0');

REPLACE INTO `blockstore_trees` VALUES ('101','120','SE');
REPLACE INTO `blockstore_trees` VALUES ('102','120','SE');
REPLACE INTO `blockstore_trees` VALUES ('103','120','SE');
REPLACE INTO `blockstore_trees` VALUES ('104','120','SE');
REPLACE INTO `blockstore_trees` VALUES ('105','120','SE');
REPLACE INTO `blockstore_trees` VALUES ('106','120','SE');
REPLACE INTO `blockstore_trees` VALUES ('107','121','SE');
REPLACE INTO `blockstore_trees` VALUES ('108','121','SE');
REPLACE INTO `blockstore_trees` VALUES ('109','121','SE');
REPLACE INTO `blockstore_trees` VALUES ('110','121','SE');
REPLACE INTO `blockstore_trees` VALUES ('111','121','SE');
REPLACE INTO `blockstore_trees` VALUES ('112','121','SE');
REPLACE INTO `blockstore_trees` VALUES ('120','0','CS');
REPLACE INTO `blockstore_trees` VALUES ('121','0','CS');

REPLACE INTO `blockstore_trees` VALUES ('201','220','SE');
REPLACE INTO `blockstore_trees` VALUES ('202','220','SE');
REPLACE INTO `blockstore_trees` VALUES ('203','220','SE');
REPLACE INTO `blockstore_trees` VALUES ('204','220','SE');
REPLACE INTO `blockstore_trees` VALUES ('205','220','SE');
REPLACE INTO `blockstore_trees` VALUES ('206','220','SE');
REPLACE INTO `blockstore_trees` VALUES ('207','221','SE');
REPLACE INTO `blockstore_trees` VALUES ('208','221','SE');
REPLACE INTO `blockstore_trees` VALUES ('209','221','SE');
REPLACE INTO `blockstore_trees` VALUES ('210','221','SE');
REPLACE INTO `blockstore_trees` VALUES ('211','221','SE');
REPLACE INTO `blockstore_trees` VALUES ('212','221','SE');
REPLACE INTO `blockstore_trees` VALUES ('220','0','CS');
REPLACE INTO `blockstore_trees` VALUES ('221','0','CS');

REPLACE INTO `blockstore_state` VALUES('120','dbox1','rz-1','2724605','1');
REPLACE INTO `blockstore_state` VALUES('121','dbox1','rz-2','2043453','1');
REPLACE INTO `blockstore_state` VALUES('220','dbox2','rz-1','2724605','0');
REPLACE INTO `blockstore_state` VALUES('221','dbox2','rz-2','2043453','0');

REPLACE into nodes VALUES ('dbox1','d2100','dbox1','testnode',NOW(),2351,NULL,'',NULL,NULL,NULL,'',NULL,NULL,'','','','','',NULL,0,1,'unknown',NULL,NULL,'fatal','none','ISUP',1349995214,NULL,NULL,'FREE_DIRTY',1349993143,0,'',NULL,2351,0,11000,11000,20000,11000,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,UUID(),0);
REPLACE into nodes VALUES ('dbox2','d2100','dbox2','testnode',NOW(),2351,NULL,'',NULL,NULL,NULL,'',NULL,NULL,'','','','','',NULL,0,2,'unknown',NULL,NULL,'fatal','none','ISUP',1349995214,NULL,NULL,'FREE_DIRTY',1349993143,0,'',NULL,2351,0,11000,11000,20000,11000,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,UUID(),0);
REPLACE into nodes VALUES ('dbox3','d2100','dbox3','testnode',NOW(),2351,NULL,'',NULL,NULL,NULL,'',NULL,NULL,'','','','','',NULL,0,3,'unknown',NULL,NULL,'fatal','none','ISUP',1349995214,NULL,NULL,'FREE_DIRTY',1349993143,0,'',NULL,2351,0,11000,11000,20000,11000,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,UUID(),0);

REPLACE into node_features VALUES ('dbox1','sanhost',0.5);
REPLACE into node_features VALUES ('dbox2','sanhost',0.5);
REPLACE into node_features VALUES ('dbox3','sanhost',0.5);

REPLACE INTO interfaces VALUES ('dbox1',0,1,'e89a8f63ec8a','155.98.38.215',NULL,NULL,'igb','eth0','ctrl','','full',0,NULL,0,0,'equal',UUID(),0);
REPLACE INTO interfaces VALUES ('dbox1',1,1,'e89a8f63ec8b','',NULL,NULL,'igb','eth1','expt','0','full',0,NULL,0,0,'equal',UUID(),0);
REPLACE INTO interfaces VALUES ('dbox2',0,1,'e89a8f63eac6','155.98.38.216',NULL,NULL,'igb','eth0','ctrl','','full',0,NULL,0,0,'equal',UUID(),0);
REPLACE INTO interfaces VALUES ('dbox2',1,1,'e89a8f63eac7','',NULL,NULL,'igb','eth1','expt','0','full',0,NULL,0,0,'equal',UUID(),0);
REPLACE INTO interfaces VALUES ('dbox3',0,1,'e89a8f63eac2','155.98.38.217',NULL,NULL,'igb','eth0','ctrl','','full',0,NULL,0,0,'equal',UUID(),0);
REPLACE INTO interfaces VALUES ('dbox3',1,1,'e89a8f63eac3','',NULL,NULL,'igb','eth1','expt','0','full',0,NULL,0,0,'equal',UUID(),0);

REPLACE INTO `interface_state` VALUES ('dbox1',0,1,'eth0',1,0,0);
REPLACE INTO `interface_state` VALUES ('dbox1',1,1,'eth1',1,0,0);
REPLACE INTO `interface_state` VALUES ('dbox2',0,1,'eth0',1,0,0);
REPLACE INTO `interface_state` VALUES ('dbox2',1,1,'eth1',1,0,0);
REPLACE INTO `interface_state` VALUES ('dbox3',0,1,'eth0',1,0,0);
REPLACE INTO `interface_state` VALUES ('dbox3',1,1,'eth1',1,0,0);

REPLACE INTO `wires` VALUES (9069,25,'Control','dbox1',0,1,'procurve2',8,11,0,0,NULL,NULL);
REPLACE INTO `wires` VALUES (2555,25,'Node','dbox1',1,1,'procurve5',10,18,0,0,NULL,NULL);
REPLACE INTO `wires` VALUES (2568,25,'Control','dbox2',0,1,'procurve2',8,12,0,0,NULL,NULL);
REPLACE INTO `wires` VALUES (2566,25,'Node','dbox2',1,1,'procurve5',10,19,0,0,NULL,NULL);
REPLACE INTO `wires` VALUES (2569,25,'Control','dbox3',0,1,'procurve2',8,10,0,0,NULL,NULL);
REPLACE INTO `wires` VALUES (2558,25,'Node','dbox3',1,1,'procurve5',10,20,0,0,NULL,NULL);

REPLACE INTO node_auxtypes VALUES ('dbox1','pcvm',100),('dbox2','pcvm',100),('dbox3','pcvm',100),('dbox1','pcsanhost',100),('dbox2','pcsanhost',100),('dbox3','pcsanhost',100);

REPLACE INTO `reserved` VALUES ('dbox1','emulab-ops','storage-pool',123456,'2012-11-28 00:45:26','dbox1','storagehost',0,'','',0,NULL,NULL,0,'none',0,0,NULL,NULL,NULL,NULL,NULL,'storage_pool');
REPLACE INTO `reserved` VALUES ('dbox2','emulab-ops','storage-pool',123456,'2012-11-28 00:45:26','dbox2','storagehost',0,'','',0,NULL,NULL,0,'none',0,0,NULL,NULL,NULL,NULL,NULL,'storage_pool');
REPLACE INTO `reserved` VALUES ('dbox3','emulab-ops','storage-pool',123456,'2012-11-28 00:45:26','dbox3','storagehost',0,'','',0,NULL,NULL,0,'none',0,0,NULL,NULL,NULL,NULL,NULL,'storage_pool');
